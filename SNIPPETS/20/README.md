20
==

## FLUIDTEMPLATE - zweiter Versuch

Hier: Auslagerung (aus ersten Vereinfachungsgründen) der Techniken in den fileadmin

WICHTIG: später soll das in der eigenen Extension landen!

## Nutzung von Bootstrap 5 Vorlage

Vorbereitetes Gitlab-Repo:

https://gitlab.com/joebrandes/bootstrap5-typo3.git

Die Lösung in `SNIPPETS/10`  hat den Ordner `fileadmin/templatingg/bu` - diese Lösung
kann einfach parallel kopiert werden in `fileadmin/templatingg/bu20`.

Danach natürlich noch das Konstanten und Setup-TS austauschen!
